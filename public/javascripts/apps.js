var NUM_SCROLL=4;
var watcher;
new Vue({
		el:"#app",
	  	data: {
	  		total : 0,
	  		products :[],
	  		cart :[],
	  		search:"cat",
	  		results:[],
	  	},
	  	methods:{
	  		addToCart:function(product){
	  			this.total +=product.price;
	  			found = false;
	  			for(var i =0;i < this.cart.length;i++){
	  				if(this.cart[i].id==product.id){
	  					this.cart[i].qty++;
	  					found=true;
	  				}
	  			}
	  			if(!found){
	  				this.cart.push({
		  				id:product.id,
		  				title:product.title,
		  				price:product.price,
		  				qty:1
		  			});
	  			}
	  			
	  		},
	  		dec:function(item){
	  			item.qty--;
	  			this.total-=item.qty;
	  			if(item.qty <=0){
	  				var i = this.cart.indexOf(item);
	  				this.cart.splice(i,1);
	  			}
	  		},
	  		inc:function(item){
	  			item.qty++;
	  			this.total+=item.qty;
	  		},
	  		onSubmit:function(){
	  			var path = "/search?q=".concat(this.search);
	  			this.$http.get(path)
	  			.then(function(res){
	  				this.results=res.body;
	  				this.products.length = [];
	  				this.appendResults();
	  			})
	  		},
	  		appendResults:function(){
	  			if(this.products.length < this.results.length){
	  				var toAppend = this.results.slice(this.products.length,NUM_SCROLL+this.products.length);
	  				this.products= this.products.concat(toAppend);
	  			}
	  		}
	  		
	  	},
	  	filters:{
	  			currency:function(price){
	  				return "$" + price.toFixed(2);
	  			}
	  		},
	  	created:function(){
	  		this.products = [];
	  		this.onSubmit();
	  	},
	  	updated:function(){
	  		var sensor = document.querySelector("#product-list-bottom");
			watcher = scrollMonitor.create(sensor);
			watcher.enterViewport(this.appendResults);
	  	},
	  	beforeUpdate:function(){
	  		if(watcher){
	  			watcher.destroy();
	  			watcher=null;
	  		}
	  	}
	});

